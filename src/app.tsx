import React from 'react';
import { Provider } from 'react-redux';

import { BrowserRouter } from 'react-router-dom';
import Dashboard from './containers/dashboard';
import { store } from './__data__/store';

const App = () => (
    <Provider store={store}>
        <BrowserRouter basename="/mp_transport">
            <Dashboard />
        </BrowserRouter>
    </Provider>
);

export default App;
