import React from 'react';

import style from './style.css';
import cn from 'classnames';
import { StatusStep } from '../../components';

interface stepProps {
    className?: string;
    step: number;
    text: string;
}

const Step: React.FC<stepProps> = ({ className, step, text }) => (
    <div className={cn(style.step)}>
        <StatusStep className={className}>{step}</StatusStep>
        <span>{text}</span>
    </div>
);

export default Step;
