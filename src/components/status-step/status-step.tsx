import React from 'react';

import style from './style.css';
import cn from 'classnames';

interface statusStepProps {
    className?: string;
}

const StatusStep: React.FC<statusStepProps> = ({ className, children, ...rest }) => (
    <div {...rest} className={cn(style.main, className)}>
        {children}
    </div>
);

export default StatusStep;
