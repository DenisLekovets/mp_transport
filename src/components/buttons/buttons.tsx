import React from 'react';
import style from './style.css';
import cn from 'classnames';

interface ButtonsProps
    extends React.DetailedHTMLProps<
        React.ButtonHTMLAttributes<HTMLButtonElement>,
        HTMLButtonElement
    > {
    className?: string;
    borderScheme?: 'border_none';
    onClick?: () => void;
}

const Buttons: React.FC<ButtonsProps> = ({ borderScheme, children, className, ...rest }) => (
    <button {...rest} className={cn(style.main, style[borderScheme], className)}>
        {children}
    </button>
);

export default Buttons;
