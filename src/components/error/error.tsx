import React from 'react';
import style from './style.css';

type ErrorProps = {
    error?: {
        text: string;
        title?: string;
    };
};

const Error: React.FC<ErrorProps> = ({ error: { text, title } }) => (
    <div className={style.main}>
        {title && <h2 className={style.errorTitle}>{title}</h2>}
        <p className={style.errorText}>{text}</p>
    </div>
);

Error.defaultProps = {
    error: {
        text: 'Извините, что-то пошло не так',
    },
};

export default Error;
