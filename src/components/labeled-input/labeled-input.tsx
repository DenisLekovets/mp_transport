﻿import React from 'react';
import PropTypes from 'prop-types';
import style from './style.css';
import { FieldRenderProps } from 'react-final-form';

interface LabeledInputProps
    extends Omit<
        React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>,
        'id'
    > {
    id: string | number;
    text?: string;
    inputRef?: React.RefObject<HTMLInputElement>;
}

export const LabeledInput: React.FC<Partial<FieldRenderProps<LabeledInputProps>>> = ({
    inputRef,
    text,
    id,
    input,
    meta,
    ...rest
}) => (
    <div className={style.main}>
        {text && (
            <label
                className={
                    ((meta.error || meta.submitError) && meta.touched && style.labelError) ||
                    style.label
                }
                htmlFor={String(id)}
            >
                {text}
            </label>
        )}

        <input
            {...input}
            value={String(input.value || '')}
            className={
                ((meta.error || meta.submitError) && meta.touched && style.inputError) ||
                style.input
            }
            ref={inputRef}
            id={String(id)}
            {...rest}
        />
        {(meta.error || meta.submitError) && meta.touched && (
            <div className={style.error}>
                <span>{meta.error || meta.submitError}</span>
            </div>
        )}
    </div>
);

LabeledInput.propTypes = {
    text: PropTypes.string.isRequired,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
};

LabeledInput.defaultProps = {
    error: void 0,
};
