import React from 'react';

import style from './style.css';

const TransportPage: React.FC = ({ children }) => (
    <div className={style.wrapper}>
        <header className={style.test} />
        {children}
    </div>
);

export default TransportPage;
