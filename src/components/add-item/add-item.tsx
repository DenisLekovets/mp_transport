import React, { useState } from 'react';

import style from './style.css';
import cn from 'classnames';

interface stepProps {
    className?: string;
}

const AddItem: React.FC<stepProps> = ({ className, children }) => {
    const [type, setType] = useState('main');

    function Click() {
        if (type === 'main') {
            setType('mainAdd');
        } else {
            setType('main');
        }
    }

    return (
        <button className={cn(style[type], className)} onClick={Click}>
            {children}
        </button>
    );
};

export default AddItem;
