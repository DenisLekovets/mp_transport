import React from 'react';

import style from './style.css';
import { AddItem } from '../index';

interface itemProps {
    img: any;
    transport: string;
    capacity: string;
}

const Item: React.FC<itemProps> = ({ img, transport, capacity }) => (
    <div className={style.item}>
        <img src={img} />
        <p>{transport}</p>
        <p>{capacity}</p>
        <AddItem />
    </div>
);

export default Item;
