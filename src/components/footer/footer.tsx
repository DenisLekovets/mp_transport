import React from 'react';
import style from './style.css';
import { socialUrls } from '../../__data__/social-url';
import assets from '../../assets';
import cn from 'classnames';

const Footer = () => (
    <footer className={style.footer}>
        <div className={style.footerContent}>
            <div className={style.footerInner}>
                <div className={style.footerLogo}>
                    <div className={style.footerLogoMediapult}>
                        <span>MEDIA </span>
                        <span>
                            PULT
                            <br />
                        </span>
                    </div>
                    <div className={style.agregatorWrapper}>
                        <h6 className={style.footerAgregator}>Агрегатор оффлайн рекламы</h6>
                    </div>
                </div>
                <nav className={style.footerNavigation}>
                    <a className={style.footerLink} href="#">
                        Носители
                    </a>
                    <a className={style.footerLink} href="#">
                        Сервис
                    </a>
                    <a className={style.footerLink} href="#">
                        Контакты
                    </a>
                </nav>
                <div className={style.footerPhoneWrapper}>
                    <h2 className={style.footerPhone}>+78008886677</h2>
                </div>
            </div>
            <div className={style.footerSocialContainer}>
                {socialUrls.map((item) => (
                    <a
                        key={item.href}
                        className={cn(style.footerSocialLink, style[item.color])}
                        href={item.href}
                        target="_blank"
                        rel="noreferrer"
                    >
                        <img className={style.footerSocialIcon} src={assets[item.image]} />
                    </a>
                ))}
            </div>
        </div>
    </footer>
);

export default Footer;
