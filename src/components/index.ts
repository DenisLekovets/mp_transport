// Импортиривать все компоненты и прописать их в экспорте
import Buttons from './buttons';
import List_item from './list_item';
import Footer from './footer';
import StatusStep from './status-step';
import Step from './step';
import AddItem from './add-item';
import TransportPage from './transport-page';
import Item from './item';
import LabeledInput from './labeled-input';
import ErrorBoundary from './error-boundary';
import Error from './error';

export {
    Buttons,
    List_item,
    Footer,
    StatusStep,
    Step,
    AddItem,
    TransportPage,
    Item,
    LabeledInput,
    ErrorBoundary,
    Error,
};
