import React from 'react';
import cn from 'classnames';
import style from './style.css';

interface List_itemProps {
    className?: string;
}

const List_item: React.FC<List_itemProps> = ({ children, className, ...rest }) => (
    <div {...rest} className={cn(style.main, className)}>
        {children}
    </div>
);

export default List_item;
