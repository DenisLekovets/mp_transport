import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { URLs } from '../__data__/ad-urls';
import style from './style.css';

import Main from './main';

import TransportPage from './transport';
import { Footer } from '../components';
import Login from './login';
import Register from './register';

const Dashboard = () => (
    <div className={style.dashboard}>
        <Switch>
            <Route exact path="/">
                <Main />
            </Route>

            <Route path={URLs.login.url}>
                <Login />
            </Route>

            <Route path={URLs.register.url}>
                <Register />
            </Route>

            <Route path={URLs.radio.url}>
                <h1>Реклама на радио</h1>
            </Route>

            <Route path={URLs.elevator.url}>
                <h1>Реклама в лифтах</h1>
            </Route>

            <Route path={URLs.transport.url}>
                <TransportPage />
            </Route>

            <Route path="*">
                <Redirect to="/" />
            </Route>
        </Switch>
        <Footer />
    </div>
);
export default Dashboard;
