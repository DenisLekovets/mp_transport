import React, { useEffect, useRef, useState } from 'react';
import style from './style.css';
import { LabeledInput, Buttons, ErrorBoundary } from '../../components';
import { baseUrl, URLs } from '../../__data__/ad-urls';
import { Form, Field, FormSpy } from 'react-final-form';
import { Redirect, Link } from 'react-router-dom';
import { submitLogin } from '../../__data__/actions/login';
import { connect } from 'react-redux';
import login from '@main/__data__/store/reducers/login';

type MapStateToProps = {
    data: string;
    asyncError: boolean | string;
};

type MapDispatchToProps = {
    submitLogin: (login, password) => void;
};

type LoginProps = MapStateToProps & MapDispatchToProps;

function Login({ submitLogin, data, asyncError }: React.PropsWithChildren<LoginProps>) {
    const firstInputRef = useRef(null);
    const [needRedirerct, setNeedRedirect] = useState(false);
    const [showError, setShowError] = useState(false);

    useEffect(() => {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setNeedRedirect(true);
        }
    }, [data]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    async function handleSubmit(values) {
        const { login, password } = values;

        if (login && password) {
            localStorage.setItem('login', login);
            localStorage.setItem('password', password);
            submitLogin(login, password);
        }
    }

    if (needRedirerct || (localStorage.getItem('login') && localStorage.getItem('password'))) {
        return <Redirect to={baseUrl} />;
    }

    function formValidation(value) {
        const errors: any = {};
        if (!value.login) {
            errors.login = true;
        }
        if (!value.password) {
            errors.password = true;
        }
        return errors;
    }

    return (
        <ErrorBoundary>
            <div className={style.wrapper}>
                <Form
                    onSubmit={handleSubmit}
                    validate={formValidation}
                    render={({ handleSubmit }) => (
                        <form className={style.loginForm} onSubmit={handleSubmit}>
                            <div className={style.logo}>
                                <div className={style.mediapult}>
                                    <span>MEDIA </span>
                                    <span>
                                        PULT
                                        <br />
                                    </span>
                                </div>
                                <div className={style.agregatorWrapper}>
                                    <h6 className={style.agregator}>Агрегатор оффлайн рекламы</h6>
                                </div>
                            </div>
                            <div className={style.loginInner}>
                                <h3 className={style.loginTitle}>Вход</h3>
                                <Field
                                    name="login"
                                    component={LabeledInput}
                                    inputRef={firstInputRef}
                                    text="Введите логин"
                                    id="login-input"
                                />
                                <Field
                                    name="password"
                                    component={LabeledInput}
                                    type="password"
                                    text="Введите пароль"
                                    id="password-input"
                                />
                                {showError && <div className={style.error}>{asyncError}</div>}
                                <FormSpy
                                    subscription={{
                                        pristine: true,
                                        errors: true,
                                        submitting: true,
                                    }}
                                    render={({ pristine, errors, submitting }) => (
                                        <Buttons
                                            className={style.button}
                                            type="submit"
                                            disabled={
                                                pristine ||
                                                errors?.password ||
                                                errors?.login ||
                                                submitting
                                            }
                                        >
                                            Войти
                                        </Buttons>
                                    )}
                                />
                                <div className={style.loginRegister}>
                                    <Link to={URLs.register.url} className={style.link}>
                                        <span>Зарегистрироваться</span>
                                    </Link>
                                </div>
                                <div className={style.loginRegister}>
                                    <Link to={baseUrl} className={style.link}>
                                        <span>Отмена</span>
                                    </Link>
                                </div>
                            </div>
                        </form>
                    )}
                />
            </div>
        </ErrorBoundary>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    data: state.login.data,
    asyncError: state.login.error,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitLogin: (login, password) => dispatch(submitLogin(login, password)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
