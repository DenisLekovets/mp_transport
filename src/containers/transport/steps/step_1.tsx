import React from 'react';

import style from './style.css';
import { AddItem, Buttons, Step, Item } from '../../../components';

import assets from '../../../assets';

function Step1({ nextStep }) {
    return (
        <div>
            <div className={style.statusStep}>
                <div className={style.statusCity}>
                    <span>Город:</span>
                    <span className={style.cityOpt}>Хабаровск</span>
                </div>
                <div className={style.status}>
                    <span>Статус:</span>
                    <Step
                        step={1}
                        text={'Выберите вместимость'}
                        className={style.statusStepCheck}
                    />
                    <Step step={2} text={'Выбор маршрута'} />
                    <Step step={3} text={'Выбор нанесения'} />
                </div>
                <div className={style.statusNext}>
                    <Buttons onClick={() => nextStep()} className={style.statusButton}>
                        Далее ➞
                    </Buttons>
                </div>
            </div>
            <div className={style.stepOne}>
                <div className={style.sequence}>
                    <span>ТЕКСТ</span>
                </div>
                <div className={style.itemInfo}>
                    <Item img={assets.taxi} transport={'Такси'} capacity={'Вместимость: малая'} />
                    <Item img={assets.paz} transport={'Пазик'} capacity={'Вместимость: средняя'} />
                    <Item
                        img={assets.tramvay}
                        transport={'Трамвай'}
                        capacity={'Вместимость: большая'}
                    />
                    <Item
                        img={assets.mTaxi}
                        transport={'Маршрутное такси'}
                        capacity={'Вместимость: малая'}
                    />
                    <Item
                        img={assets.bus}
                        transport={'Автобус'}
                        capacity={'Вместимость: большая'}
                    />
                    <Item
                        img={assets.trol}
                        transport={'Тролейбус'}
                        capacity={'Вместимость: большая'}
                    />
                </div>
            </div>
        </div>
    );
}

export default Step1;
