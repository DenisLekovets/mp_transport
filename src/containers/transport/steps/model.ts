export enum TransportStep {
    STEP_1 = 'STEP_1',
    STEP_2 = 'STEP_2',
}

export type StageProps = {
    nextStep: () => void;
};
