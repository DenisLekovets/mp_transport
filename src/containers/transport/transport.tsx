import React from 'react';

import Step1 from './steps/step_1';
import Step2 from './steps/step_2';

import { TransportStep, StageProps } from './steps/model';
import { useStages } from '../../hooks';
import { TransportPage } from '../../components';

const steps = {
    [TransportStep.STEP_1]: {
        component: Step1,
        next: TransportStep.STEP_2,
    },
    [TransportStep.STEP_2]: {
        component: Step2,
        next: null,
    },
};

function Transport() {
    const [currentStep, nextStep] = useStages(steps, TransportStep.STEP_1);
    const Stage: React.FC<StageProps> = steps[currentStep].component;
    const info = steps[currentStep].info;

    return (
        <TransportPage>
            <Stage nextStep={nextStep} />
        </TransportPage>
    );
}

export default Transport;
