import React, { useEffect, useRef, useState } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { Form, Field, FormSpy } from 'react-final-form';
import { LabeledInput, Buttons, ErrorBoundary } from '../../../components';
import { baseUrl, URLs } from '../../../__data__/ad-urls';
import style from './style.css';
import { submitRegisterFormCode } from '../../../__data__/actions/register-code';
import { connect } from 'react-redux';

type MapStateToProps = {
    data: string;
    asyncError: boolean | string;
};

type MapDispatchToProps = {
    submitRegister: (code) => void;
};

type RegisterCodeProps = MapStateToProps & MapDispatchToProps;

function InputCode({
    submitRegister,
    data,
    asyncError,
}: React.PropsWithChildren<RegisterCodeProps>) {
    const firstInputRef = useRef(null);
    const [needRedirerct, setNeedRedirect] = useState(false);
    const [showError, setShowError] = useState(false);

    useEffect(function () {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setNeedRedirect(true);
        }
    }, [data]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    async function handleSubmit(values) {
        const { code } = values;
        if (code) {
            localStorage.setItem('code', code);
            submitRegister(code);
        }
    }

    if (needRedirerct) {
        return <Redirect to={URLs.login.url} />;
    }

    function formValidation(value) {
        const errors: any = {};
        if (!value.code) {
            errors.code = true;
        }
        return errors;
    }

    return (
        <div className={style.wrapper}>
            <Form
                onSubmit={handleSubmit}
                validate={formValidation}
                render={({ handleSubmit }) => (
                    <form className={style.loginForm} onSubmit={handleSubmit}>
                        <div className={style.logo}>
                            <div className={style.mediapult}>
                                <span>MEDIA </span>
                                <span>
                                    PULT
                                    <br />
                                </span>
                            </div>
                            <div className={style.agregatorWrapper}>
                                <h6 className={style.agregator}>Агрегатор оффлайн рекламы</h6>
                            </div>
                        </div>
                        <div className={style.loginInner}>
                            <h3 className={style.loginTitle}>Подтверждение e-mail</h3>
                            <Field
                                name="code"
                                component={LabeledInput}
                                inputRef={firstInputRef}
                                text="Введите код"
                                id="code-input"
                            />
                            {showError && <div className={style.error}>{asyncError}</div>}
                            <FormSpy
                                subscription={{
                                    pristine: true,
                                    errors: true,
                                    submitting: true,
                                }}
                                render={({ pristine, errors, submitting }) => (
                                    <Buttons
                                        className={style.button}
                                        type="submit"
                                        disabled={pristine || errors?.code || submitting}
                                    >
                                        Продолжить
                                    </Buttons>
                                )}
                            />
                            <div className={style.cancel}>
                                <Link to={baseUrl} className={style.link}>
                                    <span>Отмена</span>
                                </Link>
                            </div>
                        </div>
                    </form>
                )}
            />
        </div>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    data: state.register.inputCode.data,
    asyncError: state.register.inputCode.error,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitRegister: (code) => dispatch(submitRegisterFormCode(code)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputCode);
