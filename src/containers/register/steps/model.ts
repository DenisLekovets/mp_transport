export enum RegisterStep {
    DATA_STEP,
    CODE_STEP,
}

export type StageProps = {
    setStep: (step: RegisterStep) => void;
};
