import React, { useEffect, useRef, useState } from 'react';
import { Form, Field, FormSpy } from 'react-final-form';
import { LabeledInput, Buttons } from '../../../components';
import { baseUrl } from '../../../__data__/ad-urls';
import { RegisterStep, StageProps } from './model';
import style from './style.css';
import { Link } from 'react-router-dom';
import { submitRegisterFormData } from '../../../__data__/actions/register-data';
import { connect } from 'react-redux';

type MapStateToProps = {
    data: string;
    asyncError: boolean | string;
};

type MapDispatchToProps = {
    submitRegister: (login, password, email) => void;
};

type RegisterDataProps = StageProps & MapStateToProps & MapDispatchToProps;

function InputData({
    setStep,
    submitRegister,
    data,
    asyncError,
}: React.PropsWithChildren<RegisterDataProps>) {
    const firstInputRef = useRef(null);
    const [showError, setShowError] = useState(false);
    const [needStep, setNeedStep] = useState(false);

    useEffect(function () {
        firstInputRef.current.focus();
    }, []);

    useEffect(() => {
        if (data) {
            setNeedStep(true);
        }
    }, [data]);

    useEffect(() => {
        if (asyncError) {
            setShowError(true);
        }
    }, [asyncError]);

    async function handleSubmit(values) {
        const { login, password, passwordRepeat, email } = values;
        if (login && password && passwordRepeat && email) {
            localStorage.setItem('login', login);
            localStorage.setItem('password', password);
            localStorage.setItem('passwordRepeat', passwordRepeat);
            localStorage.setItem('email', email);
            submitRegister(login, password, email);
        }
    }

    if (needStep) {
        setStep(RegisterStep.CODE_STEP);
    }

    function formValidation(value) {
        const errors: any = {};
        const min = 6;
        if (!value.login) {
            errors.login = true;
        }
        if (value.password?.length < min) {
            errors.password = `Пароль должен содержать минимум ${min} символов`;
        } else if (
            /[a-z]/.test(value.password) == false ||
            /[$?&.\-_]/.test(value.password) == false
        ) {
            errors.password = `Пароль должен содержать символы латиницы и 
            символы $?&.-_`;
        }
        if (/^[\w\-.]{2,64}@[a-z]{2,16}\.[a-z]{2,3}$/.test(value.email) == false) {
            errors.email = 'E-mail не валиден';
        }
        if (!value.email) {
            errors.email = true;
        }
        if (!(value.password === value.passwordRepeat)) {
            errors.passwordRepeat = 'Пароли не совпадают';
        }
        if (!value.password) {
            errors.password = true;
        }
        if (!value.passwordRepeat) {
            errors.passwordRepeat = true;
        }
        return errors;
    }

    return (
        <div className={style.wrapper}>
            <Form
                onSubmit={handleSubmit}
                validate={formValidation}
                render={({ handleSubmit }) => (
                    <form className={style.loginForm} onSubmit={handleSubmit}>
                        <div className={style.logo}>
                            <div className={style.mediapult}>
                                <span>MEDIA </span>
                                <span>
                                    PULT
                                    <br />
                                </span>
                            </div>
                            <div className={style.agregatorWrapper}>
                                <h6 className={style.agregator}>Агрегатор оффлайн рекламы</h6>
                            </div>
                        </div>
                        <div className={style.loginInner}>
                            <h3 className={style.loginTitle}>Регистрация</h3>
                            <Field
                                name="login"
                                component={LabeledInput}
                                inputRef={firstInputRef}
                                text="Введите логин"
                                id="login-input"
                            />
                            <Field
                                name="password"
                                type="password"
                                component={LabeledInput}
                                text="Введите пароль"
                                id="password-input"
                            />
                            <Field
                                name="passwordRepeat"
                                type="password"
                                component={LabeledInput}
                                text="Повторите пароль"
                                id="password-repeat-input"
                            />
                            <Field
                                name="email"
                                component={LabeledInput}
                                text="Введите e-mail"
                                id="email-input"
                            />
                            {showError && <div className={style.error}>{asyncError}</div>}
                            <FormSpy
                                subscription={{
                                    pristine: true,
                                    errors: true,
                                    submitting: true,
                                }}
                                render={({ pristine, errors, submitting }) => (
                                    <Buttons
                                        className={style.button}
                                        type="submit"
                                        disabled={
                                            pristine ||
                                            errors?.password ||
                                            errors?.passwordRepeat ||
                                            errors?.login ||
                                            errors?.email ||
                                            submitting
                                        }
                                    >
                                        Продолжить
                                    </Buttons>
                                )}
                            />
                            <div className={style.cancel}>
                                <Link to={baseUrl} className={style.link}>
                                    <span>Отмена</span>
                                </Link>
                            </div>
                        </div>
                    </form>
                )}
            />
        </div>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    data: state.register.inputData.data,
    asyncError: state.register.inputData.error,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    submitRegister: (login, password, email) =>
        dispatch(submitRegisterFormData({ login, password, email })),
});

export default connect(mapStateToProps, mapDispatchToProps)(InputData);
