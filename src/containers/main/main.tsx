import React, { useEffect, useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import style from './style.css';
import assets from '../../assets';
import { URLs } from '../../__data__/ad-urls';
import { Buttons, List_item } from '../../components';
import i18next from 'i18next';
import { getHelloPhrase } from '../../__data__/actions/main';

type MapStateToProps = {
    phrase: string;
};
type MapDispatchToProps = {
    getHelloPhrase: () => void;
};
type MainProps = MapStateToProps & MapDispatchToProps;

function Main({ phrase, getHelloPhrase }: React.PropsWithChildren<MainProps>) {
    console.log(phrase);
    const [needAuth, setNeedAuth] = useState(false);
    const [needReg, setNeedReg] = useState(false);

    useEffect(() => {
        getHelloPhrase();
    }, []);

    if (needAuth) return <Redirect to={URLs.login.url} />;
    if (needReg) return <Redirect to={URLs.register.url} />;

    return (
        <div className={style.wrapper}>
            <header className={style.header}>
                <div className={style.headerContent}>
                    <div className={style.logo}>
                        <img src={assets.logo} />
                        <span>{i18next.t('mp.transport.main.logoHeader')}</span>
                    </div>
                    <nav>
                        <p>{i18next.t('mp.transport.main.headerNavСarrier')}</p>
                        <p>{i18next.t('mp.transport.main.headerNavService')}</p>
                        <p>{i18next.t('mp.transport.main.headerNavContacts')}</p>
                    </nav>
                    <div className={style.authorization}>
                        <Buttons
                            className={style.authorizationButton}
                            onClick={() => setNeedAuth(true)}
                        >
                            {i18next.t('mp.transport.main.authorization')}
                        </Buttons>
                        <Buttons
                            className={style.authorizationButtonBorder}
                            onClick={() => setNeedReg(true)}
                        >
                            {i18next.t('mp.transport.main.registration')}
                        </Buttons>
                    </div>
                </div>
            </header>
            <div className={style.wrap}>
                <p>{i18next.t('mp.transport.main.definition')}</p>
                <span>{i18next.t('mp.transport.main.tagline')}</span>
                <div className={style.check}>
                    <Link to={URLs.radio.url}>
                        <p>{i18next.t('mp.transport.main.radio')}</p>
                    </Link>
                    <Link to={URLs.transport.url}>
                        <p>{i18next.t('mp.transport.main.transport')}</p>
                    </Link>
                    <Link to={URLs.elevator.url}>
                        <p>{i18next.t('mp.transport.main.elevator')}</p>
                    </Link>
                </div>
                <iframe className={style.video} src={'https://www.youtube.com/embed/o_fRkhIrJBM'} />
            </div>
            <div className={style.information}>
                <p>
                    {i18next.t('mp.transport.main.definition2p1')} <br />
                    {i18next.t('mp.transport.main.definition2p2')}
                </p>

                <div className={style.union}>
                    <img src={assets.info} className={style.picture} />
                    <div className={style.info}>
                        <div className={style.infoSelect}>
                            <List_item className={style.infoSelectNumber}>1</List_item>
                            <span>{i18next.t('mp.transport.main.info1')}</span>
                        </div>
                        <div className={style.infoSelect}>
                            <List_item className={style.infoSelectNumber}>2</List_item>
                            <span>{i18next.t('mp.transport.main.info2')}</span>
                        </div>
                        <div className={style.infoSelect}>
                            <List_item className={style.infoSelectNumber}>3</List_item>
                            <span>{i18next.t('mp.transport.main.info3')}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className={style.ad_selection}>
                <a className={style.ad_sel} href={'/mp_transport' + URLs.radio.url}>
                    <img src={assets.ad_sel1} className={style.ad_sel_icon} />
                    <span>{i18next.t('mp.transport.main.advertising')}</span>
                    <span className={style.span}>{i18next.t('mp.transport.main.on_radio')}</span>
                </a>
                <a className={style.ad_sel} href={'/mp_transport' + URLs.elevator.url}>
                    <img src={assets.ad_sel2} className={style.ad_sel_icon} />
                    <span>{i18next.t('mp.transport.main.advertising')}</span>
                    <span className={style.span}>{i18next.t('mp.transport.main.on_elevator')}</span>
                </a>

                <a className={style.ad_sel} href={'/mp_transport' + URLs.transport.url}>
                    <img src={assets.ad_sel3} className={style.ad_sel_icon} />
                    <span>{i18next.t('mp.transport.main.advertising')}</span>
                    <span className={style.span}>
                        {i18next.t('mp.transport.main.on_transport')}
                    </span>
                </a>
            </div>
            <div className={style.mediapult_ads}>
                <p>{i18next.t('mp.transport.main.advantages')}</p>
                <div className={style.mediapult_pictures}>
                    <div className={style.mediapult_picture1}>
                        <img src={assets.ads1} alt={'первая картинка'} />
                        <p>{i18next.t('mp.transport.main.advantages1')}</p>
                    </div>
                    <div className={style.mediapult_picture1}>
                        <img src={assets.ads2} alt={'вторая картинка'} />
                        <p>{i18next.t('mp.transport.main.advantages2')}</p>
                    </div>
                    <div className={style.mediapult_picture1}>
                        <img src={assets.ads3} alt={'третья картинка'} />
                        <p>{i18next.t('mp.transport.main.advantages3')}</p>
                    </div>
                    <div className={style.mediapult_picture1}>
                        <img src={assets.ads4} alt={'четвертая картинка'} />
                        <p>{i18next.t('mp.transport.main.advantages4')}</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = (state): MapStateToProps => ({
    phrase: state.main.phrase,
});

const mapDispatchToProps = (dispatch): MapDispatchToProps => ({
    getHelloPhrase: () => dispatch(getHelloPhrase()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
