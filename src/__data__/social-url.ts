export const socialUrls = [
    { href: 'https://vk.com/', image: 'vk', color: 'vk' },
    { href: 'https://twitter.com/', image: 'twitter', color: 'twitter' },
    { href: 'https:/www.youtube.com/', image: 'youtube', color: 'youtube' },
    {
        href: 'https://ru-ru.facebook.com/',
        image: 'facebook',
        color: 'facebook',
    },
    {
        href: 'https://www.google.com/intl/ru/account/about/',
        image: 'googleplus',
        color: 'googleplus',
    },
    { href: 'https://www.instagram.com/', image: 'insta', color: 'insta' },
    { href: 'https://ru.linkedin.com/', image: 'linkedin', color: 'linkedin' },
    { href: 'https://ok.ru/', image: 'ok', color: 'ok' },
];
