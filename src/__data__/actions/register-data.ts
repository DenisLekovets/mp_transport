﻿import * as types from '../action-types';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.REGISTER_INPUT_DATA.SUBMIT });
const successActionCreator = (data) => ({ type: types.REGISTER_INPUT_DATA.SUCCESS, data });
const failureActionCreator = (error) => ({
    type: types.REGISTER_INPUT_DATA.FAILURE,
    error,
});

export const submitRegisterFormData = ({ login, password, email }) => async (dispatch) => {
    const rawData = { login, password, email };
    const baseApiUrl = getConfigValue('mp_transport.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(`${baseApiUrl}/register/data`, rawData);
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(failureActionCreator(error?.response?.data?.error || 'Неизвестная ошибка'));
    }
};
