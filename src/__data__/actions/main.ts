import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

import * as types from '../action-types';

const requestActionCreator = () => ({ type: types.MAIN.REQUEST });
const successActionCreator = (data) => ({ type: types.MAIN.SUCCESS, payload: data });
const errorActionCtreator = (error) => ({ type: types.MAIN.FAILURE, payload: error });

export const getHelloPhrase = () => async (dispatch) => {
    const baseApiUrl = getConfigValue('mp_transport.api');

    dispatch(requestActionCreator());
    try {
        const response = await axios.get(`${baseApiUrl}/hello`);
        dispatch(successActionCreator(response.data.phrase));
    } catch (error) {
        dispatch(errorActionCtreator(error?.response?.data?.error || 'Неизвестная ошибка'));
    }
};
