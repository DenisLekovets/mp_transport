﻿import * as types from '../action-types';
import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

const submitActionCreator = () => ({ type: types.REGISTER_INPUT_CODE.SUBMIT });
const successActionCreator = (data) => ({ type: types.REGISTER_INPUT_CODE.SUCCESS, data });
const failureActionCreator = (error) => ({
    type: types.REGISTER_INPUT_CODE.FAILURE,
    error,
});

export const submitRegisterFormCode = ({ code }) => async (dispatch) => {
    const rawData = { code };
    const baseApiUrl = getConfigValue('mp_transport.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(`${baseApiUrl}/register/code`, rawData);
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(failureActionCreator(error?.response?.data?.error || 'Неизвестная ошибка'));
    }
};
