﻿import axios from 'axios';
import { getConfigValue } from '@ijl/cli';

import * as types from '../action-types';

const submitActionCreator = () => ({ type: types.LOGIN.SUBMIT });
const successActionCreator = (data) => ({ type: types.LOGIN.SUCCESS, payload: data });
const errorActionCtreator = (error) => ({ type: types.LOGIN.FAILURE, payload: error });

export const submitLogin = (login, password) => async (dispatch) => {
    const baseApiUrl = getConfigValue('mp_transport.api');

    dispatch(submitActionCreator());
    try {
        const response = await axios.post(`${baseApiUrl}/login`, { login, password });
        dispatch(successActionCreator(response.data));
    } catch (error) {
        dispatch(errorActionCtreator(error?.response?.data?.error || 'Неизвестная ошибка'));
    }
};
