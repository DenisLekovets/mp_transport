import { getNavigations } from '@ijl/cli';

const navigations = getNavigations('mp_transport');

export const baseUrl = navigations['mp_transport'];
export const URLs = {
    radio: {
        url: navigations['link.mp_transport.radio'],
    },
    elevator: {
        url: navigations['link.mp_transport.elevator'],
    },
    transport: {
        url: navigations['link.mp_transport.transport'],
    },
    login: {
        url: navigations['link.mp_transport.login'],
    },
    register: {
        url: navigations['link.mp_transport.register'],
    },
};
