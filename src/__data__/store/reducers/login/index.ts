﻿import * as types from '../../../action-types';

export type LoginState = {
    data: string;
    loading: boolean;
    error: boolean | string;
};

const initialState: LoginState = {
    loading: false,
    error: false,
    data: '',
};

const handleSubmit = (state, action) => ({ ...state, loading: true, error: false });
const handleSuccess = (state, action) => ({ ...state, loading: false, data: action.payload });
const handleFailure = (state, action) => ({ ...state, loading: false, error: action.payload });

export default function (state = initialState, action) {
    switch (action.type) {
        case types.LOGIN.SUBMIT:
            return handleSubmit(state, action);
        case types.LOGIN.SUCCESS:
            return handleSuccess(state, action);
        case types.LOGIN.FAILURE:
            return handleFailure(state, action);
        default:
            return state;
    }
}
