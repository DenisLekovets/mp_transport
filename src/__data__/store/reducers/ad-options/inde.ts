import * as types from '../../../action-types';

export type MainState = {
    adOptions: any;
    loading: boolean;
    error: boolean;
};

const initialState: MainState = {
    loading: false,
    error: false,
    adOptions: [],
};

const handleRequest = (state, action) => ({ ...state, loading: true, error: false });
const handleSuccess = (state, action) => ({ ...state, loading: false, phrase: action.payload });
const handleFailure = (state, action) => ({ ...state, loading: false, error: action.payload });

export default function (state = initialState, action) {
    switch (action.type) {
        case types.MAIN.REQUEST:
            return handleRequest(state, action);
        case types.MAIN.SUCCESS:
            return handleSuccess(state, action);
        case types.MAIN.FAILURE:
            return handleFailure(state, action);
        default:
            return state;
    }
}
