﻿import * as types from '../../../action-types';

export type RegisterCodeState = {
    data: string;
    loading: boolean;
    error: string | boolean;
};

const initialState: RegisterCodeState = {
    loading: false,
    error: false,
    data: '',
};

const handleSubmit = (state, action) => ({ ...state, loading: true, error: false });
const handleSuccess = (state, action) => ({ ...initialState, loading: false, data: action.data });
const handleFailure = (state, action) => ({ ...state, loading: false, error: action.error });

export default function (state = initialState, action) {
    switch (action.type) {
        case types.REGISTER_INPUT_CODE.SUBMIT:
            return handleSubmit(state, action);
        case types.REGISTER_INPUT_CODE.SUCCESS:
            return handleSuccess(state, action);
        case types.REGISTER_INPUT_CODE.FAILURE:
            return handleFailure(state, action);
        default:
            return state;
    }
}
