import { combineReducers } from 'redux';

import mainReducer, { MainState } from './main';
import loginReducer, { LoginState } from './login';
import registerDataReducer, { RegisterDataState } from './register/input-data';
import registerCodeReducer, { RegisterCodeState } from './register/input-code';

export type AppStore = {
    main: MainState;
    login: LoginState;
    register: {
        inputData: RegisterDataState;
        inputCode: RegisterCodeState;
    };
};

export default combineReducers<AppStore>({
    main: mainReducer,
    login: loginReducer,
    register: combineReducers({
        inputData: registerDataReducer,
        inputCode: registerCodeReducer,
    }),
});
