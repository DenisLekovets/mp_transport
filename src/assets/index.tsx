import facebook from './social-icons/facebook.png';
import googleplus from './social-icons/googleplus.png';
import insta from './social-icons/insta.png';
import linkedin from './social-icons/linkedin.png';
import twitter from './social-icons/twitter.png';
import vk from './social-icons/vk.png';
import youtube from './social-icons/youtube.png';
import ok from './social-icons/ok.png';
import logo from './images/logo.png';
import ad_sel1 from './images/adsel1.png';
import ad_sel2 from './images/adsel2.png';
import ad_sel3 from './images/adsel3.png';
import btnPlus from './images/btn_Plus.svg';
import checkMark from './images/CheckMark.svg';
import taxi from './images/taxi.png';
import paz from './images/paz.png';
import tramvay from './images/tramvay.png';
import mTaxi from './images/mTaxi.png';
import bus from './images/bus.png';
import trol from './images/trol.png';
import ads1 from './images/ads1.png';
import ads2 from './images/ads2.png';
import ads3 from './images/ads3.png';
import ads4 from './images/ads4.png';
import info from './images/info.png';

export default {
    facebook,
    googleplus,
    insta,
    linkedin,
    twitter,
    vk,
    youtube,
    ok,
    ad_sel1,
    ad_sel2,
    ad_sel3,
    ads1,
    ads2,
    ads3,
    ads4,
    logo,
    btnPlus,
    checkMark,
    taxi,
    paz,
    tramvay,
    mTaxi,
    bus,
    trol,
    info,
};
