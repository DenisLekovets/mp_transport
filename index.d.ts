declare module '*.css' {
    interface Style {
        [key: string]: string;
    }

    const style: Style;

    export default style;
}

declare module '*.png' {
    const value: string;

    export default value;
}

declare module '*.otf' {
    const value: string;
}
declare module '*.jpg';

declare module '*json' {
    const value: any;

    export default value;
}

declare module '*.svg';
