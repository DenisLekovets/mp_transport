const router = require("express").Router();

const TWO_SECONDS = 2000;

const wait = (time = TWO_SECONDS) => (req, res, next) =>
    setTimeout(next, time);

router.get('/hello', wait(), (req, res) => {
    res.send(require('./mocks/main/success'));
});

router.post('/login', wait(), (req, res) => {
    res.send('ok');
});

router.post('/register/data', wait(), (req, res) => {
    res.send('ok');
});

router.post('/register/code', wait(), (req, res) => {
    res.send('ok');
});

module.exports = router;